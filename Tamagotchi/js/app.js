//Class is not fully supported yet
class Tamagotchi {
    //constructs this object - helps set it up
    constructor(name, type, energy, selector){
        this.name = name;
        this.type = type;
        this.energy = energy;
        this.element = document.querySelector(selector);

        //start tamagotchi simulation
        setInterval(this.update.bind(this), 1000);

    }

    report(){
        console.log(this.name + " is a " + this.type + " type and has " + this.energy + " energy.");
    }

    //add energy to tamagotchi
    eat(){
        //add 10 energy
        this.energy += 10;
    }

    //common video game method
    //runs the tamagotchi 'sim'
    update(){

        if(this.energy <= 0){
            this.element.innerHTML = "Your " + this.name + " has run away. :(";
        }else{
            this.energy --;
            this.element.innerHTML = "Your " + this.name + " has " + this.energy + " energy.";
        }
    }
}

//Make instance of Tamagotchi
var myTamagotchi = new Tamagotchi("Druid", "heart", 24, '#dvInfo');
myTamagotchi.report(); //have the tamagotchi expose some debug data