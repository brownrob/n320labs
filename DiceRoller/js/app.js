var die1 = 0;
var die2 = 0;
var die3 = 0;
var die4 = 0;
var die5 = 0;
var die6 = 0;

Vue.component('dice-roller', {
   template: "<div class='rolls'><div class='number'>{{currentRoll}}</div> <button v-on:click='rollDie'>Roll Die</button> </div>",
    data: function(){
       return{
           currentRoll: 0
       }
    },
    methods: {
        rollDie: function(){
            this.currentRoll = (1 + Math.floor(Math.random() * 6));

            if(this.currentRoll !== 1){
                if(this.currentRoll !== 2){
                    if(this.currentRoll !== 3){
                        if(this.currentRoll !== 4){
                            if(this.currentRoll !== 5){
                                die6 ++;
                            }else{
                                die5 ++;
                            }
                        }else{
                            die4 ++;
                        }
                    }else{
                        die3 ++;
                    }
                }else{
                    die2 ++;
                }
            }else{
                die1 ++;
            }

            $("#1").html(die1);
            $("#2").html(die2);
            $("#3").html(die3);
            $("#4").html(die4);
            $("#5").html(die5);
            $("#6").html(die6);
        }
    }
});

new Vue({
    el: "#app"
});